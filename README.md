# iptables

A wrapper module around firewall and firewall_multi modules from Forge.
Technically should be a profile, but since it is used a lot, made it a module.
implements ssh defence rules dear to the networking team.

## Parameters

* `rules_pre` - rules at the top of the stack.
* `rules_pre_ipv6` - the same for IPv6
* `rules_post` - rules at the bottom of the stack
* `rules_post_ipv6` - the same for IPv6
* `rules` - the rest of the rules
* `chains` - additional iptables chains
* `ssh_defence` - turn ssh defence on/off
* `ssh_defence_sec` - ssh defence time interval
* `ssh_defence_limit` - ssh defence hit count

## Managed default chains

Module manages default `INPUT` and `OUTPUT` tables only to set `purge` parameter to *true*. That ensures that all manually created firewall rules in these chains are removed by puppet. To change this default, put the following in a relevant hiera data source:

```yaml
iptables::chains:
  'INPUT:filter:IPv4':
    purge: false
  'INPUT:filter:IPv6':
    purge: false
```

For simplicity instead of the actual chain names in the format of the `puppetlabs-firewall` module aliases *input*, *input6*, *output*, *output6* can be used like this:

```yaml
iptables::chains:
  input:
    purge: false
  input6:
    purge: false
```

## Excluding particular chains and rules

There are applications which want to manage iptables firewall themselves creating their own rules and chains and altering them while running. Good examples of such applications are *docker* and *fail2ban*. In its default configuration `iptables` module would ensure that these rules are removed, which may not be desired. To avoid such conflicts use the `iptables::chains` paramter to fine tune the behavior of the module. The paramters of the `firewallchain` resource are described in the [documentation](https://forge.puppet.com/modules/puppetlabs/firewall/reference#firewallchain) of the *Firewall* module on Forge.

## Examples

Allow ssh access from public VPN

```yaml
iptables::rules:
  ssh:
    name: "200 ssh VPN"
    chain: INPUT
    source:
      - '171.66.16.0/21'
      - '171.66.24.0/21'
      - '171.66.176.0/20'
    proto: 'tcp'
    dport: 22
    action: 'accept'
```

Configure iptables for particular fail2ban jails

```yaml
iptables::chains:
  input:
    ignore:
      - '-j f2b-apache-dos'
      - '-j f2b-apache-badbots'
      - '-j f2b-apache-overflows'
  apache-dos:
    name: 'f2b-apache-dos:filter:IPv4'
    ensure: present
    purge: false
  apache-badbots:
    name: 'f2b-apache-badbots:filter:IPv4'
    ensure: present
    purge: false
  apache-overflows:
    name: 'f2b-apache-overflows:filter:IPv4'
    ensure: present
    purge: false
```
