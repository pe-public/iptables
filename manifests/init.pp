# Pull firewall rule and chain definitions from hiera and create resources
# out of them. Can be used as a resource in a manifest.
#
# Optionally creates ssh defence iptables rules. Note that adding it
# implies ssh being accessible to the world. 
#
# TODO: Add ssh defence code for IPv6

class iptables (
  $rules_pre = {},
  $rules_post = {},
  $rules_pre_ipv6 = {},
  $rules_post_ipv6 = {},
  $rules = {},
  $chains = {},
  $ssh_defence = false,
  $ssh_defence_secs = 300,
  $ssh_defence_limit = 5,
) {
  # enable firewall
  include firewall

  # check if the server has public IPv6 address
  if $facts['networking']['network6'] == undef {
    $public_ipv6 = false
  } else {
    $public_ipv6 = ($facts['networking']['network6'] =~ /^2607:f6d0/)
  }

  # Normalize the resource hashes so that the name of the chain is
  # in the name of the resource rather than as a separate 'name' key.
  # This ensures that the module does not error out if the same chain
  # is defined twice in different ways.
  # Remove IPv6 chains if we do not have IPv6 support.
  #
  $_chains = $chains.reduce({}) |$m,$v| {
    if 'name' in $v[1] {
      $chain_name = $v[1]['name']
    } else {
      $chain_name = $v[0]
    }

    if (! $public_ipv6) and ($chain_name =~ ':IPv6$') { $m } else {
      $m + { $chain_name => $v[1].filter |$k| { $k[0] != 'name' } }
    }
  }

  # Create firewall chains
  create_resources('firewallchain', $_chains)

  # Create the ipv4 rules at the top of the stack
  create_resources('firewall', $rules_pre)

  # Create the ipv4 rules at the bottom of the stack
  create_resources('firewall', $rules_post)

  # Check if a server has a public ipv6 address
  if $public_ipv6 {
    # Create the ipv6 rules at the top of the stack
    create_resources('firewall', $rules_pre_ipv6)

    # Create the ipv6 rules at the bottom of the stack
    create_resources('firewall', $rules_post_ipv6)
  }

  # Create the rest of the rules
  create_resources('firewall_multi', $rules)

  # Turn ssh defence on if requested
  if $ssh_defence {
    # create a hash with the parameters of the class to
    # merge with the hiera lookup, which takes precedence
    $opts = {
      log_attempts => {
        rseconds  => $ssh_defence_secs,
        rhitcount => $ssh_defence_limit,
      },
      drop_connections => {
        rseconds  => $ssh_defence_secs,
        rhitcount => $ssh_defence_limit,
      }
    }

    $opts_merged = deep_merge(lookup('iptables::rules_ssh_defence'), $opts)

    create_resources('firewallchain', lookup('iptables::sshscan_chain'))
    create_resources('firewall', $opts_merged)

    # engage ssh defence for ipv6
    if $public_ipv6 {
      $opts_ipv6 = {
        log_ipv6_attempts => {
          rseconds  => $ssh_defence_secs,
          rhitcount => $ssh_defence_limit,
        },
        drop_ipv6_connections => {
          rseconds  => $ssh_defence_secs,
          rhitcount => $ssh_defence_limit,
        }
      }

      $opts_merged_ipv6 = deep_merge(lookup('iptables::rules_ssh_defence_ipv6'), $opts_ipv6)

      create_resources('firewallchain', lookup('iptables::sshscan_chain_ipv6'))
      create_resources('firewall', $opts_merged_ipv6)
    }
  }
}
